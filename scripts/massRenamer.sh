#!/usr/bin/env bash
# Script name: massRenamer

message=$1
i=1
echo $message
for f in * ; do
    echo $f " > " ${message//#/$i}
    mv "$f" ${message//#/$i}
    ((i=i+1))
done