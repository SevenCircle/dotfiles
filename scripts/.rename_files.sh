#!/bin/bash
# $1 - name of the 

move(){
    i=$1

    for f in *.*; do
        if [[ ! "$f" == "$2 ["* ]]; then
            ext="${f##*.}"
            mv -- "$f" "$2 [${i}].$ext" && i=$((i+1))
        fi
    done
}

i=$(find . -name $1' *' | wc -l )
i=$((i+1))

[[ -z "$i" ]] && i=1

move $i "$1"