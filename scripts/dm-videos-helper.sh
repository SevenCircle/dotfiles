#!/usr/bin/env bash

# Script name: dm-youtube

# pipefail setup
set -euo pipefail

_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && cd "$(dirname "$(readlink "${BASH_SOURCE[0]}" || echo ".")")" && pwd)"
# _path="/usr/bin"
if [[  -f "${_path}/_dm-helper.sh" ]]; then
  # shellcheck disable=SC1090,SC1091
  source "${_path}/_dm-helper.sh"
else
  # shellcheck disable=SC1090
  echo "No helper-script found"
fi

# script will not hit this if there is no config-file to load
# shellcheck disable=SC1090
source "$(get_config)"


# checks if client Id already exists if not creates it
CheckClientIds(){
  if [ "${empty_arr_client_ids}" == 1 ] || [ -z "${arr_client_ids["${_channel_id}"]+x}" ]
    then
      sed -i 's/empty_arr_client_ids=1/empty_arr_client_ids=0/' "${CONFIG_FILE}"
      _user_id="$(curl --location --request GET "https://api.twitch.tv/helix/users?login=${_channel_id}" --header "CLIENT-ID: ${TWITCHCLIENTID}" --header "Authorization: ${TWITCHTOKEN}" | jq .data[0].id)"
      _user_id="${_user_id//\"}"
      echo "arr_client_ids[$1]=\"${_user_id}\"" >> "$CONFIG_FILE"
      
      arr_client_ids[$1]="${_user_id}"
  fi
}

# Gets the token value
GetToken(){
  _secretKeyURL="https://id.twitch.tv/oauth2/token?client_id=${TWITCHCLIENTID}&client_secret=${TWITCHSECRET}&grant_type=client_credentials"
  _token="Bearer $(curl -X POST ${_secretKeyURL} | jq -r .access_token)"
}

# Populates Token Variable
# PARAMS
# $1 - In case it is the value 1 it will to get a new token and print it out ot config file
RefreshToken(){
  if  [ -z "${TWITCHTOKEN+x}" ]
  then
    GetToken
    echo "TWITCHTOKEN=\"${_token}\"">> "${CONFIG_FILE}"
  elif [ "$1" -eq 1 ]
  then
    prevToken=${TWITCHTOKEN}
    GetToken
    sed -i s/"$prevToken"/"$_token"/g  "${CONFIG_FILE}"
  fi
}

# Does a curl Get Request with Twitch Headers
GetTwitchCurl(){
  _response="$(curl --location --request GET "{$1}" --header "CLIENT-ID: ${TWITCHCLIENTID}" --header "Authorization: ${TWITCHTOKEN}" | jq .)"

  if [ "$(echo "$_response" | jq -r .error)" = "Unauthorized" ]
  then
    RefreshToken 1
    GetTwitchCurl
  fi
}

main() {
  local _feed_url _channel _video
  local _channel_id _video_id _video_list
  
  _source=$(printf '%s\n' "Youtube" "Twitch" "Local" | sort | ${DMENU} 'Source:' "$@" )
  
  if [[ ${_source} == *"Youtube"* ]]; then
    _channel=$(printf '%s\n' "${!youtube_channels[@]}" | sort | ${DMENU} 'Select Channel:' "$@")
    DMENU="${DMENU:0:8} 1 ${DMENU:10:70}"

    _channel_id=$(curl -s -f -L "${youtube_channels[${_channel}]}" | grep -o "channel_id=.*" | sed 's/".*//g')
    _feed_url="https://www.youtube.com/feeds/videos.xml?channel_id=${_channel_id##*=}"
    _video_list=$(parse_rss "$(curl -s "${_feed_url}")" )
    _video=$(printf '%s\n' "${_video_list}" | sort -r | ${DMENU} 'Select Video' "$@")
    _video_id=$(echo "${_video}" | cut -d'|' -f2 | sed -e 's/^[ \t]*//')
    if [[ -n ${_video_id} ]]; then
      ${DMBROWSER} "https://www.youtube.com/watch?v=${_video_id}"
    fi
  elif [[ ${_source} == *"Twitch"* ]]; then
    _channel=$(printf '%s\n' "${!twitch_channels[@]}" | sort | ${DMENU} 'Select Channel:' "$@")
    DMENU="${DMENU:0:8} 1 ${DMENU:10:70}"

    _channel_id="$(cut -d'/' -f 4 <<< "${twitch_channels[${_channel}]//moderator\//}")"
    value=$(printf '%s\n' "No" "Yes" | sort | ${DMENU} 'Watch Vod' "$@" )
    if [ $value == "Yes" ];
    then
      RefreshToken 0
      CheckClientIds "$_channel_id"
      GetTwitchCurl "https://api.twitch.tv/helix/videos?user_id=${arr_client_ids["$_channel_id"]}&type=archive"
      _length=($(echo "${_response}" | jq '.data | length'))
      readarray -t _vods < <(echo ${_response//|} | jq '.data[] | .published_at+" | "+.title+" | "+.duration+" | "+ (.url/"/" | .[4])' -r)
      _video=$(printf '%s\n' "${_vods[@]}" | sort -r | ${DMENU} 'Select Video' "$@" | cut -d'|' -f 4 | cut -d' ' -f 2)
      ${DMBROWSER} "https://www.twitch.tv/videos/${_video}"
    elif [ $value == "No" ];
    then
       ${DMBROWSER} "${twitch_channels[${_channel}]}"
    fi
  elif [[ ${_source} == *"Local"* ]]; then
    _directory=$(printf '%s\n' "${!local_folder[@]}" | sort | ${DMENU} 'Select Vod:' "$@")
    # _vod=$(find ${local_folder[${_directory}]} -type f -exec file -N -i -- {} + | grep video | sed 's/^\(.*\):.*$/\1/' | sed 's|.*/||' | ${DMENU} 'Select Video' "$@" )
    _vod_list=$(find ${local_folder[${_directory}]} -type f -exec file -N -i -- {} + | grep video | sed 's/^\(.*\):.*$/\1/' | awk /./)
    _vod=$(printf '%s\n' "${_vod_list}" | sed 's|.*/||' | ${DMENU} 'Select Video' "$@" )

    # for vod in "${_vod_list[@]}"
    # do
    #  if [[ ${vod} == *"${_vod}"* ]]; then
    #      ${VODPLAYER} ${vod}
    #  fi
    # done

    for i in "${_vod_list[@]}"
    do
      echo "$i\n"
      # or do whatever with individual element of the array
    done
  fi

}

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@"
