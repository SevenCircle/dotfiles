#!/usr/bin/env bash
# Script name: rdupes

CreateDupeFile(){
  $(fdupes -r . > dupes.txt)
}

main(){
  input="dupes.txt"
  pwd=$(pwd)
  i=0
  while IFS= read -r line
  do
      if [ ! -z "$line" ]; then
        if [ $i == 1 ]; then
          dupe="${pwd}${line#?}"
          echo $dupe
          rm "${dupe}"
        fi
        ((i=i+1))
      else
        ((i=0))
      fi
  done < "$input"
}

CreateDupeFile

main

CreateDupeFile