#!/bin/sh
xrandr --output HDMI-0 --off --output DP-0 --off --output DP-1 --off --output DP-2 --off --output DP-3 --off --output DP-4 --primary --mode 1920x1080 --pos 1080x420 --rotate normal --output DP-5 --off --output DVI-D-1-0 --off --output HDMI-1-0 --off --output DP-1-0 --off --output DP-1-1 --off
