#!/usr/bin/env bash
# Script name: randomgreeting
# pipefail setup

readarray arr < <(jq '.[].greet' ~/Documents/greetfile/greets.json)
key=$(($RANDOM%${#arr[@]}))
value=${arr[$key]}

for (( i=0; i<${#value}; i++ )); do
  key=${value:$i:1}
  case $key in
    [[:space:]])
    xdotool key space
    ;;

    .)
    xdotool key period
    ;;   
    
    !)
    xdotool key exclam
    ;;

    \?)
    xdotool key question
    ;;
    ,)
    xdotool key comma
    ;;
    
    

    *)
      xdotool key $key
      ;;
esac

  # [[ $key != '"' ]] && [[ $key != ' ' ]] && 
  # [[ $key == ' ' ]] && 
done
xdotool key Enter