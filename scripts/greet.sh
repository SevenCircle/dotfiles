#!/usr/bin/env bash
# Script name: randomgreeting

# Function to check if any value in greets.json is zero
check_zero_values() {
  # Read arguments
  local json_content="$1"
  local length="$2"

  # Initialize a flag to indicate if any value is not zero
  local zero_flag=0

  # Loop through the Greets JSON array
  for ((index=0; index<length; index++)); do
    # Extract the current value
    local value=$(echo "$json_content" | jq -r ".[$index].value")
    
    if [ "$value" -eq 0 ]; then
      zero_flag=1
      break
    fi
    done

    if [ $zero_flag -eq 0 ]; then
      # Update the JSON content to set all values to zero
      json_content=$(echo "$json_content" | jq '.[].value = 0')
      echo "$json_content" > $file_path
      modified=1
    else
      modified=0
    fi

   echo "$modified" 
}

# Greets Json File
file_path="/home/sevencircle/MEGA/dotfiles/scripts/greets.json"

#Json Content
json_content=$(cat $file_path)
length=$(echo "$json_content" | jq 'length')

file_modified=$(check_zero_values "$json_content" "$length")

if [ $file_modified -eq 1 ]; then
  # Read the greets.json file again to get the updated content
  json_content=$(cat $file_path)
fi

value=1
while [ $value -gt 0 ]; do
  key=$(($RANDOM%${length}))
  greet=$(echo "$json_content" | jq -r ".[$key].greet")
  value=$(echo "$json_content" | jq -r ".[$key].value")
  
  new_value=$((value + 1))
  json_content=$(echo "$json_content" | jq ".[$key].value = $new_value")

done
echo "$json_content" > $file_path

xdotool type --clearmodifiers "$greet" && xdotool key --clearmodifiers Return