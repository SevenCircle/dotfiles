#Copy from vault 
sudo mv /etc/media/WWE/* /home/sevencircle/WWE/
sudo mv /etc/media/Movies/* /home/sevencircle/Movies/
sudo mv /etc/media/Pictures/* /home/sevencircle/Pictures/
sudo mv /etc/media/Series/* /home/sevencircle/Series/
sudo mv /etc/media/UFC/* /home/sevencircle/UFC/
sudo mv /etc/media/Videos/* /home/sevencircle/Videos/
sudo mv /etc/media/Box/* /home/sevencircle/Box/


#Give Reads permissions to files in proper folders
sudo chown -R jellyfin:jellyfin /home/sevencircle/WWE/
sudo chown -R jellyfin:jellyfin /home/sevencircle/Movies/
sudo chown -R jellyfin:jellyfin /home/sevencircle/Pictures/
sudo chown -R jellyfin:jellyfin /home/sevencircle/Series/
sudo chown -R jellyfin:jellyfin /home/sevencircle/UFC/
sudo chown -R jellyfin:jellyfin /home/sevencircle/Videos/
sudo chown -R jellyfin:jellyfin /home/sevencircle/Box/