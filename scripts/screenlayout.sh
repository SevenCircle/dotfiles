#!/bin/sh
xrandr --output HDMI-0 --mode 1920x1080 --pos 0x0 --rotate left --output DP-0 --off --output DP-1 --off --output DP-2 --mode 1920x1080 --pos 3000x0 --rotate right --output DP-3 --off --output DP-4 --primary --mode 1920x1080 --pos 1080x420 --rotate normal --output DP-5 --off --output DVI-D-1-0 --off --output HDMI-1-0 --off --output DP-1-0 --off --output DP-1-1 --off
