source = $HOME/.config/hypr/themes/theme.conf

#############################
### MONITORS && WORKSPACE ###
#############################

monitor=DP-1,2560x1440@180,0x0,1
monitor=DP-2,1920x1080@144,0x-1080,1
monitor=DP-3,1920x1080@240,2560x0,1
monitor=HDMI-A-1,1920x1080@60,-1920x0,1

workspace=6, monitor:DP-2, default:true
workspace=2, monitor:DP-1, default:true

#################
### AUTOSTART ###
#################

exec-once = nm-applet &
exec-once = waybar & hyprpaper
exec-once = hypridle
exec-once = volumeicon
exec-once = dunst
#exec-once = discord-canary
exec-once = flatpak run com.core447.StreamController -b
exec-once = discord
exec-once = megasync
exec-once = jamesdsp -t

#############################
### ENVIRONMENT VARIABLES ###
#############################

env = XCURSOR_SIZE,12
env = HYPRCURSOR_SIZE,5


###################
###  VARIABLES  ###
###################

$terminal = kitty
$menu = fuzzel -T kitty -o DP-1 
$fileManager = nemo
$browswer = brave --password-store=detect
$systemCommands = parallel -u ::: '$terminal -T BTop -e btop' '$terminal -T NVtop -e radeontop' '$terminal -e cava' '$terminal'
$locking = hyprlock
$calculator = qalculate-gtk


#####################
### LOOK AND FEEL ###
#####################
general { 
    gaps_in = 6
    gaps_out = 8

    border_size = 2

    # https://wiki.hyprland.org/Configuring/Variables/#variable-types for info about colors
    col.active_border = $white
    col.inactive_border = $gray

    # Set to true enable resizing windows by clicking and dragging on borders and gaps
    resize_on_border = false 

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false

    layout = dwindle
}

decoration {
    rounding = 10

    # Change transparency of focused and unfocused windows
    active_opacity = 1.0
    inactive_opacity = 1.0

    shadow {
      enabled = true
      range = 4
      render_power = 3
      color = rgba(1a1a1aee)
    }
    blur {
        enabled = true
        size = 3
        passes = 2
        vibrancy = 0.1696
    }
}

animations {
    enabled = true

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = overshot,0.05,0.9,0.1,1.1

    animation = windows, 1, 9, overshot
    animation = windowsOut, 1, 9, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 100, default, loop
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default, slidefadevert 20%
    animation = specialWorkspace, 1, 6, default, slidefadevert 20%
}


dwindle {
    pseudotile = true # Master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = true # You probably want this
}

master {
    new_status = slave
}

misc { 
    force_default_wallpaper = 0 # Set to 0 or 1 to disable the anime mascot wallpapers
    disable_hyprland_logo = true # If true disables the random hyprland logo / anime girl background. :(
}


#############
### INPUT ###
#############

input {
    kb_layout = us
    kb_variant =
    kb_model = 
    kb_options =
    kb_rules = 
    repeat_rate = 25
    repeat_delay=300
    follow_mouse = 1

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.

    touchpad {
        natural_scroll = false
    }
}

gestures {
    workspace_swipe = false
}

###################
### KEYBINDINGS ###
###################
$mainMod = SUPER

#hyperland binds
bind = $mainMod, Q, killactive,
bind = $mainMod SHIFT, Q, exit,
bind = $mainMod, F, togglefloating,
bind = $mainMod, Space, fullscreen, 0
bind = $mainMod, D, togglesplit,
bind = $mainMod, F12, exec, sh /usr/local/bin/launch.sh

#Software binds
bind = $mainMod, T, exec, $terminal
bind = $mainMod, E, exec, $fileManager
bind = $mainMod, P, exec, [workspace 3;] $systemCommands
bind = $mainMod, B, exec, $browswer
bind = $mainMod, C, exec, $calculator
bind = $mainMod SHIFT, l, exec, $locking
bind = $mainMod, R, exec, $menu
bind = $mainMod, V, exec, dm-videos
bind = ,Print, exec, slurp | grim -g - - | wl-copy

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, k, movefocus, u

# Move focus with mainMod + arrow keys (VIM)
bind = $mainMod, h, movefocus, l
bind = $mainMod, j, movefocus, r
bind = $mainMod, down, movefocus, d
bind = $mainMod, l, movefocus, d

# Move workspace with mainMod + arrow keys
bind = $mainMod CTRL, left, movecurrentworkspacetomonitor, l
bind = $mainMod CTRL, right, movecurrentworkspacetomonitor, r
bind = $mainMod CTRL, up, movecurrentworkspacetomonitor, u
bind = $mainMod CTRL, down, movecurrentworkspacetomonitor, d

# Move workspace with mainMod + arrow keys (VIM)
bind = $mainMod CTRL, h, movecurrentworkspacetomonitor, l
bind = $mainMod CTRL, j, movecurrentworkspacetomonitor, r
bind = $mainMod CTRL, k, movecurrentworkspacetomonitor, u
bind = $mainMod CTRL, l, movecurrentworkspacetomonitor, d

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bind = $mainMod SHIFT, left, movewindow, l
bind = $mainMod SHIFT, right, movewindow, r
bind = $mainMod SHIFT, up, movewindow, u
bind = $mainMod SHIFT, down, movewindow, d

# Move/resize windows with mainMod + LMB/RMB and dragging (VIM)
bindm = $mainMod, mouse:272, movewindow
bind = $mainMod SHIFT, h, movewindow, l
bind = $mainMod SHIFT, j, movewindow, r
bind = $mainMod SHIFT, k, movewindow, u
bind = $mainMod SHIFT, l, movewindow, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

#Special Workspaces
bind = $mainMod, M, togglespecialworkspace, magic
bind = $mainMod SHIFT, M, movetoworkspace, special:magic

# Tab between monitors
bind = $mainMod, Tab, focusmonitor, +1
bind = $mainMod SHIFT, Tab, focusmonitor, -1

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

binde = $mainMod ALT, down, resizeactive, 0 10
binde = $mainMod ALT, up, resizeactive, 0 -10
binde = $mainMod ALT, left, resizeactive, -10 0
binde = $mainMod ALT, right, resizeactive, 10 0

bindm = $mainMod, mouse:273, resizewindow
bindm = $mainMod, mouse:273, resizewindow

# Laptop multimedia keys for volume and LCD brightness
bindel = ,XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
bindel = ,XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
bindel = ,XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
bindel = ,XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle
bindel = ,XF86MonBrightnessUp, exec, brightnessctl s 10%+
bindel = ,XF86MonBrightnessDown, exec, brightnessctl s 10%-

##############################
### WINDOWS AND WORKSPACES ###
##############################
windowrulev2 = suppressevent maximize, class:.*

#Chat
windowrulev2 = opacity 0.8 0.8,class:^(discord)$
windowrulev2 = workspace 6, class:^(discord)$

#Dev Tools
windowrulev2 = opacity 0.8 0.8,class:^(VSCodium)$
windowrulev2 = opacity 0.8 0.8,class:^(codium)$
windowrulev2 = workspace 1, class:^(VSCodium)$
windowrulev2 = workspace 1, class:^(codium)$

#Browser
windowrulev2 = syncfullscreen 0,class:(brave-browser)
windowrulev2 = workspace 2, class:^(brave-browser)$

#Files
windowrulev2 = opacity 0.8 0.8,class:^(nemo)$
windowrulev2 = workspace 4, class:^(nemo)$
windowrulev2 = opacity 0.8 0.8,class:^(thunar)$
windowrulev2 = workspace 4, class:^(thunar)$

#GAMES
windowrulev2 = workspace 5, class:^(steam)$
windowrulev2 = workspace 5, class:^(net.lutris.Lutris)$
windowrulev2 = workspace 5, class:^(lutris)$
windowrulev2 = workspace 5, class:^(Minecraft 1.21)$
windowrulev2 = workspace 5, class:^(battle.net.exe)$
windowrulev2 = workspace 5, class:^(hearthstone.exe)$

#Music
windowrulev2 = workspace 7, class:^(Spotify)$
windowrulev2 = workspace 7, class:^(YouTube Music)$

#Vods
windowrulev2 = workspace 8, class:^(mpv)$
windowrulev2 = workspace 8, class:^(vlc)$
windowrulev2 = workspace 8, title:^(Jellyfin Media Player)$

#GPX
windowrulev2 = workspace 9, class:^(OrcaSlicer)$
windowrulev2 = workspace 9, class:^(Gimp)$

#Pictures
windowrulev2 = workspace 10, class:^(feh)$

#Floating windows
windowrulev2 = float, class:^(MEGAsync)$
windowrulev2 = float, class:^(qalculate-gtk)$
windowrulev2 = float, title:^(Select Files)$
windowrulev2 = float, title:^(Asset Manager)$
windowrulev2=noblur,class:^()$,title:^()$
