module Colors.SnakeWhite where

import XMonad

colorScheme = "snake-white"

colorBack = "#000000"
colorFore = "#bbc2cf"

color01 = "#1c1f24"
color02 = "#ff6c6b"
color03 = "#98be65"
color04 = "#da8548"
color05 = "#74848e"
color06 = "#C9C9C9"
color07 = "#5699af"
color08 = "#202328"
color09 = "#5b6268"
color10 = "#da8548"
color11 = "#4db5bd"
color12 = "#ecbe7b"
color13 = "#3071db"
color14 = "#a9a1e1"
color15 = "#E8D3E3"
color16 = "#dfdfdf"

colorTrayer :: String
colorTrayer = "--tint 0x333333"
